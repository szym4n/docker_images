#!/bin/bash

# add local user
NEW_UID=${LOCAL_UID:-1000}
NEW_USERNAME=$USER_NAME

useradd --shell /bin/bash -u $NEW_UID -o -c "" -m $NEW_USERNAME

cd /home/$NEW_USERNAME

exec gosu $NEW_USERNAME "$@"
